import java.util.Arrays;

import javax.management.timer.Timer;

public class sortluis{
    int[] list;
    int[] original;
    Timer t = new Timer();
    int n;
    public sortluis(int[] a){
        list =a;
        n = list.length;
    }
    //------------------------------------------bubble sort-------------------------------
    public void bubble(){
        long startTime = System.currentTimeMillis();
        System.out.println("Bubble");
        //System.out.println(Arrays.toString(list));
        int[] ordered = list;        
        if(ordered.length>0){
            for(int i=1;i<n;i++){
                int val = ordered[i];
                int pos=i;
                for(int j=i;j>=0;j--){
                    if(val<ordered[j]){
                        //System.out.println("change:" + val +" " + ordered[j]);
                        int tem = ordered[j];
                        ordered[j]=val;
                        ordered[pos] =tem;
                        pos=j;
                        //System.out.println(Arrays.toString(ordered));
                    }
                }
                
            }
            
            //System.out.println(Arrays.toString(ordered));
            long endTime = System.currentTimeMillis() - startTime; // tiempo en que se ejecuta su for
            System.out.println(endTime + " milisecons");
        }
    }
    //-----------------------------------Insertion sort-----------------------------------
    public void insertion(){
        long startTime = System.currentTimeMillis();
        System.out.println("Insertion");
        //System.out.println(Arrays.toString(list));
        int[] temp = list;
        for(int i=1;i<n;i++){
            int val = temp[i];
            int j = i-1;
            while(j>=0 && temp[j]>val){
                temp[j+1] = temp[j];
                j=j-1;
            }
            temp[j+1]=val;
        }
        //System.out.println(Arrays.toString(temp));
        long endTime = System.currentTimeMillis() - startTime; // tiempo en que se ejecuta su for
        System.out.println(endTime + " milisecons");

    }   
    //------------------------------------Selection sort-----------------------------------
    public void selection(){
        long startTime = System.currentTimeMillis();
        System.out.println("Selection");
        //System.out.println(Arrays.toString(list));
        int[] ordered = list;
        int[] orderedfinal = new int[n];
        for(int i=0;i<n;i++){
            int min=i;
            for(int j=i+1;j<n;j++){
                if(ordered[j]<ordered[min]){
                    min = j;
                }
            }
            int temp = ordered[min];
            ordered[min] = ordered[i];
            orderedfinal[i] =temp;
        }
        //System.out.println(Arrays.toString(orderedfinal));
        long endTime = System.currentTimeMillis() - startTime; // tiempo en que se ejecuta su for
        System.out.println(endTime + " milisecons");
    }
    //-------------------------------------------merge sort ----------------------------------------
    // using a portion of the code of Rajat Mishra
    public void mergesort(){
        long startTime = System.currentTimeMillis();
        System.out.println("Merge");
       // System.out.println(Arrays.toString(list));
        int[] ordered = list;
        this.sortmerge(ordered, 0, n-1);
        //System.out.println(Arrays.toString(ordered));
        long endTime = System.currentTimeMillis() - startTime; // tiempo en que se ejecuta su for
        System.out.println(endTime + " milisecons");
    }
    void merge(int arr[], int l, int m, int r)
    {
        int n1 = m - l + 1;
        int n2 = r - m;

        int L[] = new int [n1];
        int R[] = new int [n2];

        for (int i=0; i<n1; ++i)
            L[i] = arr[l + i];
        for (int j=0; j<n2; ++j)
            R[j] = arr[m + 1+ j];

        // Initial indexes of first and second subarrays
        int i = 0, j = 0;

        // Initial index of merged subarry array
        int k = l;
        while (i < n1 && j < n2)
        {
            if (L[i] <= R[j])
            {
                arr[k] = L[i];
                i++;
            }
            else
            {
                arr[k] = R[j];
                j++;
            }
            k++;
        }

        /* Copy remaining elements of L[] if any */
        while (i < n1)
        {
            arr[k] = L[i];
            i++;
            k++;
        }

        /* Copy remaining elements of R[] if any */
        while (j < n2)
        {
            arr[k] = R[j];
            j++;
            k++;
        }
    }
    //sort and join the array
    void sortmerge(int arr[], int l, int r)
    {
        if (l < r)
        {
            // Find the middle point
            int m = (l+r)/2;

            // Sort first and second halves
            sortmerge(arr, l, m);
            sortmerge(arr , m+1, r);

            // Merge the sorted halves
            merge(arr, l, m, r);
        }
    }
    //---------------------------------------------Quick sort-------------------------------------
    public void Quicksort(){
        long startTime = System.currentTimeMillis();
        System.out.println("Quick sort");
        //System.out.println(Arrays.toString(list));
        int[] ordered = list;
        this.sortquik(ordered, 0,  n-1);
        //System.out.println(Arrays.toString(ordered));
        list = original;
        long endTime = System.currentTimeMillis() - startTime; // tiempo en que se ejecuta su for
        System.out.println(endTime + " milisecons");
    }


    int partition(int arr[], int low, int high)
    {
        int pivot = arr[high];
        int i = (low-1); 
        for (int j=low; j<high; j++)
        {
            if (arr[j] <= pivot)
            {
                i++;
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
        int temp = arr[i+1];
        arr[i+1] = arr[high];
        arr[high] = temp;

        return i+1;
    }

    void sortquik(int arr[], int low, int high)
    {
        if (low < high)
        {
            int pi = partition(arr, low, high);
            sortquik(arr, low, pi-1);
            sortquik(arr, pi+1, high);
        }
    }
}